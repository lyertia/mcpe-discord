
### Kaynaklar

- Base Class https://github.com/barbecue/base-class-v12



### Kurulum

- Modüllerin kurulması için NodeJS ve Python cihazınızda kurulu olmalı.
- Modülleri indirmek için `npm install`
- Config dosyasını doldurduktan sonra botu çalıştırmak için `node .`



### Notlar

- OySorguyu deneyemedim ihtimali azda olsa hata oluşabilir, bana ulaşınız.
- Ban komutu sunucunuzdaki kişiye ban atmaz yalnızca oyuncular bilgi alabilsin diye yapılmıştır. Yetkili, bir oyuncuyu sunucuda yasaklarsa oyuncuların bunu discord üzerinden sorgulaması için yetkili kişi `.ban` komutunu kullanarak banı bota kayıt ettirmeli.

### Komutlar

- `.oysorgu` Kullanıcı adı belirtilen oyuncunun sunucuya oy verip vermediğini gösterir.
- `.bansorgu` Kullanıcı adı belirtilen oyuncunun banlanıp banlanmadığını gösterir.
- `.sunucu` Sunucu bilgisini verir.
- `.ban` Kullanıcı adı belirtilen oyuncunun banını bota kaydettirir.
